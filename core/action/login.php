<?php
    include("../model/UsuarioData.php");
    include("../model/AccesosCorrectosData.php");
    include("../model/AccesosFallidosData.php");
    include("../database/Incluir.php");
    $booleano = TRUE;
    $email = ssql($_POST['email']);
    $password = ssql($_POST['password']);
    $usuario = UsuarioData::getByEmail($email); 
    if(is_null($usuario) || empty($usuario))
    {
        $booleano = FALSE;
    }
    else
    {
        $navegador = $_SERVER['HTTP_USER_AGENT'];
        $host = gethostbyaddr($_SERVER['REMOTE_ADDR']);
        
            if($booleano)
            {
                //El usuario si existe, necesitamos validar el password
                if($usuario['status'] == "1")
                {
                    if (password_verify($password, $usuario['password']))
                    {
                        $accesosCorrectos = new AccesoCorrectoData();
                        $accesosCorrectos->idUsuario = $usuario['idUsuario'];
                        $accesosCorrectos->ip = $_SERVER['REMOTE_ADDR'];
                        $accesosCorrectos->navegador = $navegador;
                        $accesosCorrectos->host = $host;  
                        $accesosCorrectos->add();
                                             
                        $_SESSION['idUsuario'] = $usuario['idUsuario'];
                        $_SESSION['idEmpleado'] = $usuario['idEmpleado'];
                        $_SESSION['email'] = $usuario['email'];
                        $_SESSION['sessionid'] = "'".$ip.$navegador.$host."'";    
                    }
                    else
                    {
                        //Si el password no coincide
                        $p = 'N/A';
                        $accesosFallidos = new AccesoFallidoData();
                        $accesosFallidos->email = $email;
                        $accesosFallidos->password = $p;
                        $accesosFallidos->navegador = $navegador;
                        $accesosFallidos->host = $host;
                        $accesosFallidos->add();
                        $booleano = FALSE;
                    }
                }
                else
                {
                    //Si no esta activo
                    $accesosFallidos = new AccesoFallidoData();
                    $accesosFallidos->email = $email;
                    $accesosFallidos->password = $password;
                    $accesosFallidos->navegador = $navegador;
                    $accesosFallidos->host = $host;
                    $accesosFallidos->add();
                    $booleano = FALSE;
                }
            }
            else
            {
                //Ingresar el registro en accesosf                
                $accesosFallidos = new AccesoFallidoData();
                $accesosFallidos->email = $email;
                $accesosFallidos->password = $password;
                $accesosFallidos->navegador = $navegador;
                $accesosFallidos->host = $host;
                $accesosFallidos->add();
                $booleano = FALSE;
            }
        
    }
    
    if ($booleano) 
    {
        $response = array(
            "mensaje" => "Acceso correcto",
            "status" => "ok"
        );
    }
    else
    {
        $response = array(
            "mensaje" => "<b>Usuario y/o password incorrectos</b>.",
            "status" => "error"
        );
    }
   
    echo json_encode($response);
?>