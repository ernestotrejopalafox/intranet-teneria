<?php

    include("../model/EmpleadoData.php");
    include("../model/PersonaData.php");
    include("../model/DireccionEmpleadoData.php");
    include("../model/UsuarioData.php");
    include("../database/Incluir.php");
    $booleano = TRUE;

    $tmp_name = $_FILES["foto"]['tmp_name'];
    $directorio_destino = "../../img/empleados";
    //si hemos enviado un directorio que existe realmente y hemos subido el archivo    
    if (is_dir($directorio_destino) && is_uploaded_file($tmp_name))
    {
        $img_file = $_FILES["foto"]['name'];
        $img_type = $_FILES["foto"]['type'];
        // Si se trata de una imagen   
        if (((strpos($img_type, "gif") || strpos($img_type, "jpeg") || strpos($img_type, "jpg")) || strpos($img_type, "png")))
        {
            //¿Tenemos permisos para subir la imágen?
            if (move_uploaded_file($tmp_name, $directorio_destino . '/' . $img_file))
            {
                $booleano = TRUE;
            }
            else {
                $booleano = false;
            }
        }
        else {
            $booleano = false;
        }
    }
    else {
        $booleano = false;
    }

    if ($booleano) 
    {
        $persona = new PersonaData();
        $persona->nombre = $_POST['nombre'];
        $persona->aPaterno = $_POST['aPaterno'];
        $persona->aMaterno = $_POST['aMaterno'];
        $persona->sexo = $_POST['sexo'];
        $persona->edoCivil = $_POST['edoCivil'];
        $idPersona = $persona->add();

        $empleado = new EmpleadoData();
        $empleado->idPersona = $idPersona;
        $empleado->idPerfil = $_POST['perfil'];
        $empleado->status = 1;
        $empleado->fechaRegistro = "now()";
        $idEmpleado = $empleado->add();

        $usuario = new UsuarioData();
        $usuario->idEmpleado = $idEmpleado;
        $usuario->email = $_POST['email'];
        $pass = substr($_POST['nombre'], 0);  // devuelve "la prmer letra"
        $pass = $pass.$_POST['aPaterno'];
        $usuario->password = password_hash($pass,PASSWORD_DEFAULT);
        $usuario->status = 1;
        $usuario->fechaRegistro = "now()";   
        $usuario->add();

        $direccionEmpleado = new DireccionEmpleadoData();
        $direccionEmpleado->idEmpleado = $idEmpleado;
        $direccionEmpleado->idEstado = $_POST['estado'];
        $direccionEmpleado->calle = $_POST['calle'];
        $direccionEmpleado->no_ext = $_POST['no_ext'];
        $direccionEmpleado->no_int = $_POST['no_int'];
        $direccionEmpleado->colonia = $_POST['colonia'];
        $direccionEmpleado->municipio = $_POST['municipio'];
        $direccionEmpleado->cp = $_POST['cp'];
        $direccionEmpleado->telefono = $_POST['telefono'];
        $direccionEmpleado->status = 1;
        $direccionEmpleado->fechaRegistro = "now()";
        $direccionEmpleado->add();
    }  
        
    if ($booleano) 
    {
        $response = array(
            "mensaje" => "Empleado registrado con exito",
            "status" => "ok"
        );
    }
    else
    {
        $response = array(
            "mensaje" => "<b>Error, vuelve a intentarlo</b>.",
            "status" => "error"
        );
    }
   
    echo json_encode($response);
?>