function SuccessMessage(mensaje) {
    color = 'success';
    $.notify({
        icon: "nc-icon nc-bell-55",
        message: "<b>" + mensaje + "</b>."

    }, {
        type: color,
        timer: 8000,
        placement: {
            from: "bottom",
            align: "right"
        }
    });
}

function NotificationMessage(mensaje) {
    color = 'primary';
    $.notify({
        icon: "nc-icon nc-bell-55",
        message: "<b>" + mensaje + "</b>."

    }, {
        type: color,
        timer: 8000,
        placement: {
            from: "bottom",
            align: "right"
        }
    });
}

function ErrorMessage(mensaje) {
    color = 'danger';
    $.notify({
        icon: "nc-icon nc-bell-55",
        message: "<b>" + mensaje + "</b>."

    }, {
        type: color,
        timer: 8000,
        placement: {
            from: "bottom",
            align: "right"
        }
    });
}