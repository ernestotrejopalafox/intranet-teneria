function IniciarSesion() {
    var url = "core/action/login.php"; // El script a dónde se realizará la petición.
    $.ajax({
        type: "POST",
        url: url,
        data: $("#frmLogin").serialize(), // Adjuntar los campos del formulario enviado.
        success: function(data) {
            var respuesta = $.parseJSON(data);
            if (respuesta.status === "ok") {
                location.href = "index.php?s=main";
            } else {
                ErrorMessage(respuesta.mensaje);
                document.getElementById("frmLogin").reset();
            }
        }
    });

    return false; // Evitar ejecutar el submit del formulario.
}

function addEmpleado() {
    var formData = new FormData(document.getElementById("frmNewEmpleado"));
    formData.append("dato", "valor");
    var url = "core/action/addEmpleado.php"; // El script a dónde se realizará la petición.
    $.ajax({
        type: "POST",
        url: url,
        processData: false,
        contentType: false,
        data: formData, // Adjuntar los campos del formulario enviado.
        success: function(data) {
            console.log(data);
            var respuesta = $.parseJSON(data);
            if (respuesta.status === "ok") {
                SuccessMessage(respuesta.mensaje);
                document.getElementById("frmNewEmpleado").reset();
            } else {
                ErrorMessage(respuesta.mensaje);
                document.getElementById("frmNewEmpleado").reset();
            }
        }
    });

    return false; // Evitar ejecutar el submit del formulario.
}