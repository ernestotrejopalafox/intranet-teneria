<?php

class Model {

	public static function exists($modelname){
		$fullpath = self::getFullpath($modelname);
		$found=false;
		if(file_exists($fullpath)){
			$found = true;
		}
		return $found;
	}

	public static function getFullpathUser($modelname){
		return "./admin/core/app/model/".$modelname.".php";
		//return Core::$root."core/app/model/".$modelname.".php";
		//return Core::$root."core/modules/index/model/".$modelname.".php";
	}

	public static function getFullpath($modelname){
		return "core/app/model/".$modelname.".php";
		//return Core::$root."core/app/model/".$modelname.".php";
		//return Core::$root."core/modules/index/model/".$modelname.".php";
	}
	
	public static function many($query,$aclass){
		$cnt = 0;
		$array = array();
		while ($r = pg_fetch_array($query, null, PGSQL_ASSOC)) {
			$array[$cnt] = new $aclass;
			$cnt2=1;
			foreach ($r as $key => $v) {
				$array[$cnt]->$key = $v;
				$cnt2++;
			}
			$cnt++;
		}
		return $array;
	}
	//////////////////////////////////
	public static function one($query,$aclass){
		$cnt = 0;
		$found = null;
		$data = new $aclass;
		while($r = pg_fetch_array($query, null, PGSQL_ASSOC)){
			$cnt=1;
			foreach ($r as $key => $v) {
				$data->$key = $v;
				$cnt++;
			}

			$found = $data;
			break;
		}
		return $found;
	}

}



?>