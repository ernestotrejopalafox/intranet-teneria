<?php
	include_once("ConfiguracionDatabase.php");	
    function sec_session_start() 
    {
        $session_name = 'stTienPC10a';
        $secure = SECURE;
        $httponly = true;
        // Obliga a las sesiones a solo utilizar cookies.
        if (ini_set('session.use_only_cookies', 1) === FALSE) 
        {
            exit('ERROR NO SE PUEDE INCIAR SESION SEGURA INISET');
        }
        // Obtiene los params de los cookies actuales.
        $cookieParams = session_get_cookie_params();
        session_set_cookie_params($cookieParams["lifetime"],
            $cookieParams["path"], 
            $cookieParams["domain"], 
            $secure,
            $httponly);

        session_name($session_name);
        session_start();            // Inicia la sesión PHP.
        session_regenerate_id();    // Regenera la sesión, borra la previa. 
    }
?>