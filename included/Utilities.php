<?php
    function fecha_corta($fecha)
    {
        $dia = substr($fecha,8,2);
        $mes = substr($fecha,5,2);
        $año = substr($fecha,0,4);
        //$hora = substr($fecha,11,9);
        $mes = mes($mes);
        $fecha = "<strong>$dia de $mes de $año</strong>";
        return $fecha;        
    }

    function fecha_h($fecha)
    {
        $dia = substr($fecha,8,2);
        $mes = substr($fecha,5,2);
        $año = substr($fecha,0,4);
        $hora = substr($fecha,11,9);
        $mes = mes($mes);
        $fecha = "<strong>$dia de $mes de $año - $hora</strong>";
        return $fecha;        
    }

    function fecha($fecha)
    {
        //2014-09-26 17:32:27
        $dia = substr($fecha,8,2);
        $mes = substr($fecha,5,2);
        $año = substr($fecha,0,4);
        $hora = substr($fecha,11,9);
        $mesf = mes1($mes);
        $fecha = "$dia de $mesf de $año $hora";
        return $fecha;    
    }

    function fechad($fecha)
    {			
        $dia = substr($fecha,8,2);
        $mes = substr($fecha,5,2);
        $año = substr($fecha,0,4);
        $hora = substr($fecha,11,9);
        $mesf = mes1($mes);
        $fechac = "$dia de $mesf de $año $hora";			
        $dias = array('Domingo', 'Lunes','Martes','Miercoles','Jueves','Viernes','Sabado'); 			
        $fechad = $dias[date('N', strtotime($fecha))]." ".$fechac;			
        return $fechad;			
    }
        
    function mes1($mes)
    {
        if($mes == 1)
        {
            return "Enero";
        }
        else if($mes == 2)
        {
            return "Febrero";
        }
        else if($mes == 3)
        {
            return "Marzo";
        }
        else if($mes == 4)
        {
            return "Abril";
        }
        else if($mes == 5)
        {
            return "Mayo";
        }
        else if($mes == 6)
        {
            return "Junio";
        }
        else if($mes == 7)
        {
            return "Julio";
        }
        else if($mes == 8)
        {
            return "Agosto";
        }
        else if($mes == 9)
        {
            return "Septiembre";
        }
        else if($mes == 10)
        {
            return "Octubre";
        }
        else if($mes == 11)
        {
            return "Noviembre";
        }
        else if($mes == 12)
        {
            return "Diciembre";
        }
        return $mes;
    }

    function mes($mes)
    {
        if($mes == 1)
        {
            return "Ene";
        }
        else if($mes == 2)
        {
            return "Feb";
        }
        else if($mes == 3)
        {
            return "Mar";
        }
        else if($mes == 4)
        {
            return "Abr";
        }
        else if($mes == 5)
        {
            return "May";
        }
        else if($mes == 6)
        {
            return "Jun";
        }
        else if($mes == 7)
        {
            return "Jul";
        }
        else if($mes == 8)
        {
            return "Ago";
        }
        else if($mes == 9)
        {
            return "Sep";
        }
        else if($mes == 10)
        {
            return "Oct";
        }
        else if($mes == 11)
        {
            return "Nov";
        }
        else if($mes == 12)
        {
            return "Dic";
        }
        return $mes;
    }

?>