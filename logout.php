<?php	
	//Cerrar sesion de forma segura
	include("core/autoload.php");
    unset($_SESSION['idUsuario']); 
    unset($_SESSION['email']); 
    unset($_SESSION['sessionid']); 
    unset($_SESSION['idEmpleado']);
    session_destroy();	
	header("Location: index.php");	
?>