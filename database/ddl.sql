-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema sgt_db
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema sgt_db
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `sgt_db` DEFAULT CHARACTER SET utf8 ;
USE `sgt_db` ;

-- -----------------------------------------------------
-- Table `sgt_db`.`perfil`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sgt_db`.`perfil` (
  `idPerfil` INT(11) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(100) NULL DEFAULT NULL,
  `descripcion` TEXT NULL DEFAULT NULL,
  `status` CHAR(1) NULL DEFAULT NULL,
  `fechaRegistro` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`idPerfil`))
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sgt_db`.`persona`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sgt_db`.`persona` (
  `idPersona` INT(11) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(60) NULL DEFAULT NULL,
  `aPaterno` VARCHAR(45) NULL DEFAULT NULL,
  `aMaterno` VARCHAR(45) NULL DEFAULT NULL,
  `sexo` CHAR(1) NULL DEFAULT NULL,
  `edoCivil` CHAR(1) NULL DEFAULT NULL,
  PRIMARY KEY (`idPersona`))
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sgt_db`.`empleado`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sgt_db`.`empleado` (
  `idEmpleado` INT(11) NOT NULL AUTO_INCREMENT,
  `idPersona` INT(11) NULL DEFAULT NULL,
  `idPerfil` INT(11) NULL DEFAULT NULL,
  `status` CHAR(1) NULL DEFAULT NULL,
  `fechaRegistro` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`idEmpleado`),
  INDEX `PersonaEmpleado_idx` (`idPersona` ASC),
  INDEX `PerfilEmpleado_idx` (`idPerfil` ASC),
  CONSTRAINT `PerfilEmpleado`
    FOREIGN KEY (`idPerfil`)
    REFERENCES `sgt_db`.`perfil` (`idPerfil`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `PersonaEmpleado`
    FOREIGN KEY (`idPersona`)
    REFERENCES `sgt_db`.`persona` (`idPersona`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sgt_db`.`usuario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sgt_db`.`usuario` (
  `idUsuario` INT(11) NOT NULL AUTO_INCREMENT,
  `idEmpleado` INT(11) NULL DEFAULT NULL,
  `email` VARCHAR(100) NULL DEFAULT NULL,
  `password` VARCHAR(100) NULL DEFAULT NULL,
  `status` CHAR(1) NULL DEFAULT NULL,
  `fecharRegistro` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`idUsuario`),
  INDEX `EmpleadoUsuario_idx` (`idEmpleado` ASC),
  CONSTRAINT `EmpleadoUsuario`
    FOREIGN KEY (`idEmpleado`)
    REFERENCES `sgt_db`.`empleado` (`idEmpleado`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sgt_db`.`accesocorrecto`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sgt_db`.`accesocorrecto` (
  `idAccesoCorrecto` INT(11) NOT NULL AUTO_INCREMENT,
  `idUsuario` INT(11) NULL DEFAULT NULL,
  `fecha` DATETIME NULL DEFAULT NULL,
  `ip` CHAR(15) NULL DEFAULT NULL,
  `navegador` VARCHAR(100) NULL DEFAULT NULL,
  `host` VARCHAR(100) NULL DEFAULT NULL,
  PRIMARY KEY (`idAccesoCorrecto`),
  INDEX `CUsuariosAccesos_idx` (`idUsuario` ASC),
  CONSTRAINT `AccesoUsuario`
    FOREIGN KEY (`idUsuario`)
    REFERENCES `sgt_db`.`usuario` (`idUsuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 7
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sgt_db`.`accesofallido`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sgt_db`.`accesofallido` (
  `idAccesoFallido` INT(11) NOT NULL AUTO_INCREMENT,
  `ip` CHAR(15) NULL DEFAULT NULL,
  `fecha` DATETIME NULL DEFAULT NULL,
  `email` VARCHAR(100) NULL DEFAULT NULL,
  `password` VARCHAR(100) NULL DEFAULT NULL,
  `navegador` TEXT NULL DEFAULT NULL,
  `host` VARCHAR(100) NULL DEFAULT NULL,
  PRIMARY KEY (`idAccesoFallido`))
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sgt_db`.`estado`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sgt_db`.`estado` (
  `idEstado` INT(11) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NULL DEFAULT NULL,
  `abrev` VARCHAR(45) NULL DEFAULT NULL,
  `fechaRegistro` DATETIME NULL DEFAULT NULL,
  `status` CHAR(1) NULL DEFAULT NULL,
  PRIMARY KEY (`idEstado`))
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sgt_db`.`direccionempleado`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sgt_db`.`direccionempleado` (
  `idDireccion` INT(11) NOT NULL AUTO_INCREMENT,
  `idEmpleado` INT(11) NULL DEFAULT NULL,
  `idEstado` INT(11) NULL DEFAULT NULL,
  `calle` VARCHAR(100) NULL DEFAULT NULL,
  `no_ext` VARCHAR(5) NULL DEFAULT NULL,
  `no_int` VARCHAR(5) NULL DEFAULT NULL,
  `colonia` VARCHAR(45) NULL DEFAULT NULL,
  `municipio` VARCHAR(45) NULL DEFAULT NULL,
  `status` CHAR(1) NULL DEFAULT NULL,
  `fechaRegistro` DATETIME NULL DEFAULT NULL,
  `cp` CHAR(5) NULL DEFAULT NULL,
  `telefono` VARCHAR(10) NULL DEFAULT NULL,
  PRIMARY KEY (`idDireccion`),
  INDEX `EstadoDomicilioEmpleado_idx` (`idEstado` ASC),
  INDEX `EmpleadoDomicilioEmpleado_idx` (`idEmpleado` ASC),
  CONSTRAINT `EmpleadoDomicilioEmpleado`
    FOREIGN KEY (`idEmpleado`)
    REFERENCES `sgt_db`.`empleado` (`idEmpleado`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `EstadoDomicilioEmpleado`
    FOREIGN KEY (`idEstado`)
    REFERENCES `sgt_db`.`estado` (`idEstado`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sgt_db`.`permisosperfil`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sgt_db`.`permisosperfil` (
  `idPermisosPerfil` INT(11) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NULL DEFAULT NULL,
  `decripcion` VARCHAR(45) NULL DEFAULT NULL,
  `status` CHAR(1) NULL DEFAULT NULL,
  `fechaRegistro` DATETIME NULL DEFAULT NULL,
  `idPerfil` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`idPermisosPerfil`),
  INDEX `PerfilPermiso_idx` (`idPerfil` ASC),
  CONSTRAINT `PerfilPermiso`
    FOREIGN KEY (`idPerfil`)
    REFERENCES `sgt_db`.`perfil` (`idPerfil`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
